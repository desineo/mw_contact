<?php

/**
 * WP Mail handler
 * @version 17.4
 * @author Matěj Rokos <matej.rokos@desineo.com>
 */

class Mailhandler {

	var $mail_subject = "Message from website";


	function quoted_printable_encode($sText,$bEmulate_imap_8bit=true) {
	  // split text into lines
	  $aLines=explode(chr(13).chr(10),$sText);

	  for ($i=0;$i<count($aLines);$i++) {
	    $sLine =& $aLines[$i];
	    if (strlen($sLine)===0) continue; // do nothing, if empty

	    $sRegExp = '/[^\x09\x20\x21-\x3C\x3E-\x7E]/e';

	    // imap_8bit encodes x09 everywhere, not only at lineends,
	    // for EBCDIC safeness encode !"#$@[\]^`{|}~,
	    // for complete safeness encode every character :)
	    if ($bEmulate_imap_8bit)
	      $sRegExp = '/[^\x20\x21-\x3C\x3E-\x7E]/e';

	    $sReplmt = 'sprintf( "=%02X", ord ( "$0" ) ) ;';
	    $sLine = preg_replace( $sRegExp, $sReplmt, $sLine );

	    // encode x09,x20 at lineends
	    {
	      $iLength = strlen($sLine);
	      $iLastChar = ord($sLine{$iLength-1});

	      //              !!!!!!!!
	      // imap_8_bit does not encode x20 at the very end of a text,
	      // here is, where I don't agree with imap_8_bit,
	      // please correct me, if I'm wrong,
	      // or comment next line for RFC2045 conformance, if you like
	      if (!($bEmulate_imap_8bit && ($i==count($aLines)-1)))

	      if (($iLastChar==0x09)||($iLastChar==0x20)) {
	        $sLine{$iLength-1}='=';
	        $sLine .= ($iLastChar==0x09)?'09':'20';
	      }
	    }    // imap_8bit encodes x20 before chr(13), too
	    // although IMHO not requested by RFC2045, why not do it safer :)
	    // and why not encode any x20 around chr(10) or chr(13)
	    if ($bEmulate_imap_8bit) {
	      $sLine=str_replace(' =0D','=20=0D',$sLine);
	      //$sLine=str_replace(' =0A','=20=0A',$sLine);
	      //$sLine=str_replace('=0D ','=0D=20',$sLine);
	      //$sLine=str_replace('=0A ','=0A=20',$sLine);
	    }

	    // finally split into softlines no longer than 76 chars,
	    // for even more safeness one could encode x09,x20
	    // at the very first character of the line
	    // and after soft linebreaks, as well,
	    // but this wouldn't be caught by such an easy RegExp
	    preg_match_all( '/.{1,73}([^=]{0,2})?/', $sLine, $aMatch );
	    $sLine = implode( '=' . chr(13).chr(10), $aMatch[0] ); // add soft crlf's
	  }

	  // join lines into text
	  return implode(chr(13).chr(10),$aLines);
  }
  
    /**
     * E-mail header encoding according to RFC 2047
     * @param string text to encode
     * @param string encoding, defaults is utf-8
     * @return string string to be used in e-mail header
     * @copyright Jakub Vrána, http://php.vrana.cz/
     */
    public function mime_header_encode($text, $encoding = "utf-8") {
        return "=?$encoding?Q?" . $this->quoted_printable_encode($text) . "?=";
    }

    /**
     * Sends the email
     *
     * @param   string  From name
     * @param   string  From
     * @param   string  Message
     * @return  bool
     */
    public function send_email($name_from, $from, $to, $message, $content = 'text/plain') {
        $headers = "MIME-Version: 1.0\r\nContent-Type: ".$content."; charset=utf-8\nContent-Transfer-Encoding: 8bit\n";
        $headers .= 'From: '.$this->mime_header_encode($name_from).'<'.$from.'>'. "\r\n";

        mail(
            $to,
            $this->mime_header_encode($this->mail_subject),
            $message,
            $headers);
        return true;
    }

    /**
     * Turns key=>value pairs to text
     */
    public function array2txt($content) {
    	$out = "\r\n";
    	foreach ($content as $key => $name) {
    		$out .= $key.":\r\n".$name."\r\n\r\n";
    	}
    	return $out;
    }

    /**
     * Turns array of key=>value pairs to html e-mail
     */
    public function array2html($content, $title = false) {
    	$out = '<html>
    	<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<meta http-equiv="Content-Style-Type" content="text/css" />
			<title>'.(($title) ? $title : $this->mail_subject).'</title>
		</head>
		<body style="text-align:center;background-color:#fff;color:#000;font-family:sans-serif;font-size:13px;" >
		<h1>'.(($title) ? $title : $this->mail_subject).'</h1>';

    	$out .= '<table style="text-align:left;border:none;">';
    	foreach ($content as $key => $name) {
    		$out .= '<tr><th style="padding:0.2em 0.5em;">'.esc_html($key).'</th>';
    		$out .= '<td style="padding:0.2em 0.5em;">'.esc_html($name).'</td></tr>';
    	}
    	$out .= '</table>
    	</body>
    	</html>';
    	return $out;
    }

}
