<?php

return '
<form action="'.get_permalink().'" method="post" id="mw_contact">
	<div id="mw_contact_status"></div>
	<div class="field">
		<label for="mw_contact_name" class="required">'.__('Your name', 'mw_contact').'</label>
		<input type="text" name="mw_contact_name" id="mw_contact_name">
	</div>
	<div class="field">
		<label for="mw_contact_email" class="required">'.__('E-mail', 'mw_contact').'</label>
		<input type="text" name="mw_contact_email" id="mw_contact_email">
	</div>
	<div class="field">
		<label for="mw_contact_message" class="arealabel required">'.__('Message', 'mw_contact').'</label>
		<textarea name="mw_contact_message" id="mw_contact_message"></textarea>
	</div>
	%note%
	<div class="sendbutton">
		<input type="submit" class="button button-primary" name="mw_contact_send" value="'.__('Send', 'mw_contact').'">
	</div>
</form>
';
