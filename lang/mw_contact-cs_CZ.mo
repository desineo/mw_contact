��          �      �       0     1     E  1   L     ~     �     �  '   �     �     �     �  &   
  	   1  n  ;     �     �  ?   �                (  3   /     c      k     �  (   �     �                            	      
                        Contact form e-mail E-mail Filled in contact forms are sent to this address. Invalid contact form e-mail. Message Name Please, check the fields and try again. Send Sending, please wait... Unauthorized request. Sorry. Your message has been sent. Thank you. Your name Project-Id-Version: MW_Contact
POT-Creation-Date: 2017-04-16 16:46+0200
PO-Revision-Date: 
Last-Translator: Matej Rokos <matej.rokos@desineo.com>
Language-Team: 
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;esc_attr_x:1,2c
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
 Kontaktní e-mail E-mail Na tuto adresu se posílají vyplněné kontaktní formuláře. Neplatný kontaktní e-mail. Zpráva Jméno Prosím, zkontrolujte formulář a zkuste to znovu. Odeslat Odesílám, počkejte prosím... Neoprávněná operace. Pardon. Vaše zpráva byla odeslána. Děkujeme. Vaše jméno 