<?php
/*
Plugin Name: Contact Form by MarbleWeb
Description: Simple contact form. Shortcode: [mw_contact note="Some note for visitors."]
Author: Matěj Rokos
Version: 17.4
Author URI: http://matejrokos.desineo.com
License: MIT
*/

require WP_PLUGIN_DIR.'/mw_contact/mailhandler.php';
require WP_PLUGIN_DIR.'/mw_contact/validace.php';


class MW_Contact {

	private $prefix = 'mw_contact';

	public function __construct() {
		add_action('plugins_loaded', array($this, 'load_textdomain'));
		add_action('admin_init', array($this, 'init_settings'));
		add_action( 'wp_enqueue_scripts', array($this, 'register_scripts'));

		add_shortcode('mw_contact', array($this, 'shortcode'));

		if (is_admin()) {
			add_action( 'wp_ajax_mw_contact_send', array($this, 'send_callback') );
			add_action( 'wp_ajax_nopriv_mw_contact_send', array($this, 'send_callback') );
		}
	}

	function load_textdomain() {
		load_plugin_textdomain( $this->prefix, false,  'mw_contact/lang' );
	}

	function init_settings() {
		add_settings_field(
			$this->prefix.'_mailto',
			__('Contact form e-mail', $this->prefix),
			array($this, 'settings_callback'),
			'general'
		);

		register_setting('general', $this->prefix.'_mailto', array($this, 'settings_validate_email'));
	}

	function settings_callback() {
		$mail = esc_attr(get_option($this->prefix.'_mailto'));
		echo '<input name="'.$this->prefix.'_mailto" id="'.$this->prefix.'_mailto" type="email" value="'.$mail.'">';
		echo '<p id="'.$this->prefix.'_mailto-description" class="description">'.__('Filled in contact forms are sent to this address.', $this->prefix).'</p>';
	}

	function settings_validate_email($string) {
		$value = filter_var($string, FILTER_VALIDATE_EMAIL);

		if ($value === false && strlen($string)) {
			add_settings_error( $this->prefix.'_mailto', 'invalid-email', __('Invalid contact form e-mail.', $this->prefix), 'error' );
		}

		return $value;
	}

	/* Register javascript */
	function register_scripts() {
		wp_register_script('mw_contact', plugins_url('mw_contact.js', __FILE__), array('jquery'), '1.0', false);

		wp_localize_script('mw_contact', 'mw_contact_vars', array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'ajax_nonce' => wp_create_nonce('mw_contact_send'),
				'sending' => __('Sending, please wait...', $this->prefix)
			)
		);
	}

	/* This function is launched when shortcode is called */
	function shortcode($atts) {
		$params = shortcode_atts( array(
	        'note' => '',
	    ), $atts );

		wp_enqueue_style('mw_contact', plugins_url().'/mw_contact/mw_contact.css');
		wp_enqueue_script('mw_contact');

		$form = include(WP_PLUGIN_DIR.'/mw_contact/form.php');

		// Wrap note in a paragraph
		if (strlen($params['note'])) {
			$params['note'] = '<p class="note">'.$params['note'].'</p>';
		}

		// Put note in form
		$form = str_replace('%note%', $params['note'], $form);

		/* Uncomment the following lines to enable non-js subscription (not recommended) */
		/*
		$sent = FALSE;

		// Get e-mail to send message to
		$mailto = get_option($this->prefix.'_mailto');

		if (isset($_POST['send']) && filter_var($mailto, FILTER_VALIDATE_EMAIL)) {
			$sent = $this->processmail($mailto);
		}

		if (isset($_POST['send']) && !$sent) {
			$form = '<p class="'.$this->prefix.'_infomessage">'.__('Please, check the fields and try again.', $this->prefix).'</p>'
					.$form;
		}

		if (isset($_POST['send']) && $sent) {
			$form = '<p class="'.$this->prefix.'_infomessage">'.__('Your message has been sent. Thank you.', $this->prefix).'</p>';
		}
		*/

		return $form;
	}

	/* Form submit callback */
	function send_callback() {
		if(!check_ajax_referer( 'mw_contact_send', '_ajax_nonce', false )) {
			wp_send_json_error(array(
				'message' => __('Unauthorized request. Sorry.', $this->prefix)
			));
			die();
		}

		$sent = FALSE;

		// Get e-mail to send message to
		$mailto = get_option($this->prefix.'_mailto');

		if (filter_var($mailto, FILTER_VALIDATE_EMAIL)) {
			$sent = $this->processmail($mailto);
		}

		if ($sent !== TRUE) {
			wp_send_json_error(array(
				'message' => __('Please, check the fields and try again.', $this->prefix)
			));
			die();
		}

		wp_send_json_success(array(
			'message' => __('Your message has been sent. Thank you.', $this->prefix)
		));
		die();

	}

	/* Process e-mail */
	function processmail($mailto) {
		$validate = array(
			'mw_contact_name' => 'string',
			'mw_contact_email' => 'email',
			'mw_contact_message' => 'string'
		);

		$valid = TRUE;

		foreach ($validate as $key => $f) {
			if (!call_user_func('Validace::check_'.$f, $_POST[$key])) {
				$valid = FALSE;
			}
		}

		if (!$valid) return FALSE;

		$mailer  = new Mailhandler();
		$msghtml = $mailer->array2html(array(
			__('Name', $this->prefix) => $_POST['mw_contact_name'],
			__('E-mail', $this->prefix) => $_POST['mw_contact_email'],
			__('Message', $this->prefix) => $_POST['mw_contact_message']
		));

		$mailer->send_email(
			Validace::clean_val($_POST['mw_contact_name']),
			Validace::clean_val($_POST['mw_contact_email']),
			$mailto,
			$msghtml,
			'text/html'
		);

		return TRUE;
	}

}

$mw_contact = new MW_Contact();
