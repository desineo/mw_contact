<?php

/**
 * Simplified validation class
 * by MarbleWeb
 * @license	MIT
 */
class Validace {

	/* vycisteni vstupu od osklivych znaku */
	public static function clean_val($val) {
		return strtr(strip_tags(trim($val)), array('&' => '', '<' => '', '>' => '', '"' => '', '\\' => '', '\'' => ''));
	}

	/* kontrola stringu, co může být prázdný */
	public static function check_stringi($val) {
		$val = self::clean_val($val);
		if (strlen($val) == 0) return TRUE;
		if (strlen($val) < 2) return FALSE;
		return TRUE;
	}

	/* kontrola stringu */
	public static function check_string($val) {
		$val = self::clean_val($val);
		if (strlen($val) < 2) return FALSE;
		return TRUE;
	}

	public static function check_email($val) {
		return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $val)) ? FALSE : TRUE;
	}

	public static function check_telefon($val) {
		$tel = str_replace(' ','', trim($val)); // vyhážeme mezery
		if (substr($tel,0,3) == '+42') $tel = substr($tel,4); // ořízneme +420 / +421
		if (is_numeric($tel) && (strlen($tel) == 9)) return TRUE;
		return FALSE;
	}

	public static function check_numeric($val) {
		if (!is_numeric($val)) return FALSE;
		return TRUE;
	}

	public static function check_numerici($val) {
		if (strlen($val) == 0) return TRUE;
		if (!is_numeric($val)) return FALSE;
		return TRUE;
	}

}
