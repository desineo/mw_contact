/* MW Contact */
jQuery(function($){
	$('#mw_contact_status').hide();

	$('#mw_contact').on('click', 'input[type=submit]', function(e) {
		e.preventDefault();
		jQuery.ajax({
			type: "post",
			url: mw_contact_vars.ajax_url,
			data: { action: 'mw_contact_send', // action
					mw_contact_name: $( '#mw_contact input[name=mw_contact_name' ).val(),
					mw_contact_email: $( '#mw_contact input[name=mw_contact_email]' ).val(),
					mw_contact_message: $( '#mw_contact textarea[name=mw_contact_message]' ).val(),
					_ajax_nonce: mw_contact_vars.ajax_nonce
			},
			beforeSend: function() {
				jQuery("#mw_contact_status").html( mw_contact_vars.sending );
			},
			success: function(result){
				if (result.success) {
					jQuery("#mw_contact").html('<div id="mw_contact_status" class="message_final">'+result.data.message+'</div>');

					if (typeof ga != "undefined") {
						ga('send', {
						  hitType: 'event',
						  eventCategory: 'Contact form',
						  eventAction: 'sent',
						  eventLabel: window.location.href
						});
					}

				}
				else {
					jQuery('#mw_contact_status').html( result.data.message );
					jQuery('#mw_contact_status').show().delay(10 * 1000).hide('slow');
				}
			},
			error: function(data) {
				console.log(data);
			}
		}); //close jQuery.ajax
	});

});
